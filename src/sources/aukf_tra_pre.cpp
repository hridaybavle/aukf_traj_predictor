﻿#include "aukf_tra_pre.h"

aukf_tra_pre::aukf_tra_pre() {}
aukf_tra_pre::~aukf_tra_pre() {}

void aukf_tra_pre::ownSetUp()
{
    received_odom_data_ = false;
    //initializing the filter
    this->init();
}

void aukf_tra_pre::ownStart()
{
    pre_tra_sub_ = n_.subscribe("/drone7/drone_predicted_pose", 1, &aukf_tra_pre::predicted_pose_callback, this);
    upd_tra_sub_ = n_.subscribe("/drone7/drone_updated_pose", 1, &aukf_tra_pre::updated_pose_callback, this);

    Lemniscate_tra_pub_ = n_.advertise<visualization_msgs::Marker>(Lemniscate_tra_, 1, true);
    predicted_tra_pub_ = n_.advertise<visualization_msgs::Marker>(predicted_tra_, 1, true);
    updated_tra_pub_ = n_.advertise<visualization_msgs::Marker>(updated_tra_, 1, true);

    Lemniscate_tra_sub_ = n_.subscribe("/gazebo/model_states", 1, &aukf_tra_pre::drone_pose_callback, this);
    drone_pre_tra_pub_ = n_.advertise<droneMsgsROS::robotPoseStampedVector>(drone_predicted_pose_, 1);
    drone_upd_tra_pub_ = n_.advertise<droneMsgsROS::dronePose>(drone_updated_pose_, 1);
}

void aukf_tra_pre::ownStop()
{
    received_odom_data_ = false;
}

void aukf_tra_pre::ownRun()
{
    std::vector<cv::Point3d> pre_vector;
    droneMsgsROS::dronePose upd_pose;
    droneMsgsROS::robotPoseStamped pre_pose;
    droneMsgsROS::robotPoseStampedVector pre_pose_vector;
    bool new_predicted_pose_ = false;

    timePrev_ = timeNow_;
    timeNow_ = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);
    deltaT_   = timeNow_ - timePrev_;

    //getting the tf
    this->getDronePoseTF();

    //double time_odom = (double) measurements_.stamp_.sec + ((double) measurements_.stamp_.nsec / (double) 1E9);
    //double duration = timeNow_ - time_odom;

    aukf_.aukfPrediction(X_aukf_, P_, F_, deltaT_);
    std::cout << "P predicted " << P_ << std::endl;


    if(received_odom_data_ /*&& duration < 0.1*/)
    {
        Eigen::VectorXd X_Measure = Eigen::VectorXd(3);
        Eigen::VectorXd X_Pre = Eigen::VectorXd(10);
        Eigen::MatrixXd P_Pre = Eigen::MatrixXd(13,13);
        Eigen::MatrixXd F_Pre = Eigen::MatrixXd(13,13);

        X_Measure(0) = measurements_.x;
        X_Measure(1) = measurements_.y;
        X_Measure(2) = measurements_.z;

        aukf_.aukfUpdate(X_aukf_,P_,X_Measure);

        upd_pose.x = X_aukf_(0);
        upd_pose.y = X_aukf_(2);
        upd_pose.z = X_aukf_(4);
        drone_upd_tra_pub_.publish(upd_pose);

        X_Pre = X_aukf_;
        P_Pre = P_;
        F_Pre = F_;

        TrajPrediction(X_Pre, P_Pre, F_Pre, deltaT_, pre_vector, limit_number_);
        new_predicted_pose_ = true;

        std::cout << "X_aukf_ " << X_aukf_ << std::endl;
        std::cout << "P_ " << P_ << std::endl;
        received_odom_data_ = false;
    }

    if(new_predicted_pose_)
    {
        for(int i = 0; i < limit_number_; i++)
        {
            pre_pose.header.stamp = ros::Time::now() + ros::Duration(i * deltaT_);
            pre_pose.x = pre_vector[i].x;
            pre_pose.y = pre_vector[i].y;
            pre_pose.z = pre_vector[i].z;
            pre_pose.theta = 0;
            pre_pose_vector.robot_pose_vector.push_back(pre_pose);
        }
        drone_pre_tra_pub_.publish(pre_pose_vector);
    }
    pre_pose_vector.robot_pose_vector.clear();
    pre_vector.clear();
}

void aukf_tra_pre::init()
{
    ros::param::get("~stack_path", stack_path_);

    if(stack_path_.length() == 0)
        stack_path_ = "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack";

    ros::param::get("~drone_id", drone_id_);

    ros::param::get("~drone_predicted_pose", drone_predicted_pose_);

    if(drone_predicted_pose_.length() == 0)
        drone_predicted_pose_ = "drone_predicted_pose";

    ros::param::get("~drone_updated_pose", drone_updated_pose_);

    if(drone_updated_pose_.length() == 0)
        drone_updated_pose_ = "drone_updated_pose";

    ros::param::get("~Lemniscate_tra", Lemniscate_tra_);

    if(Lemniscate_tra_.length() == 0)
        Lemniscate_tra_ = "Lemniscate_tra";

    ros::param::get("~predicted_tra", predicted_tra_);

    if(predicted_tra_.length() == 0)
        predicted_tra_ = "predicted_tra";

    ros::param::get("~updated_tra", updated_tra_);

    if(updated_tra_.length() == 0)
        updated_tra_ = "updated_tra";

    ros::param::get("~limit_number", limit_number_);

    if(limit_number_ == 0)
        limit_number_ = 1000;

    X_aukf_.setZero(13);
    F_.setZero(13,13);
    P_.setIdentity(13,13);
    P_.diagonal().fill(10.0);
    X_aukf_.setZero(13);
    timeNow_ = 0;
}

void aukf_tra_pre::getDronePoseTF()
{
    //check for tf
    tf::StampedTransform drone_pose_transform;
    try{
        ros::Time now = ros::Time::now();
        drone_pose_listener_.waitForTransform("/odom","/drone_close_kalman", now, ros::Duration(0.1));
        drone_pose_listener_.lookupTransform("/odom","/drone_close_kalman", now, drone_pose_transform);
        received_odom_data_ = true;

        measurements_.stamp_ = ros::Time::now();
        measurements_.x = drone_pose_transform.getOrigin().x();
        measurements_.y = drone_pose_transform.getOrigin().y();
        measurements_.z = drone_pose_transform.getOrigin().z();
    }
    catch(tf::TransformException ex){
        ROS_ERROR("%s", ex.what());
        received_odom_data_ = false;
    }
}

void aukf_tra_pre::drone_pose_callback(const gazebo_msgs::ModelStates::ConstPtr &msg)
{
    cv::Point3d mav_pose;

    for (size_t i = 0; i < msg->name.size(); i++)
    {
        if (msg->name[i].compare("moving_drone") == 0)
        {
            measurements_.stamp_ = ros::Time::now();
            measurements_.x = msg->pose[i].position.x;
            measurements_.y = msg->pose[i].position.y;
            measurements_.z = msg->pose[i].position.z;

            mav_pose.x = msg->pose[i].position.x;
            mav_pose.y = msg->pose[i].position.y;
            mav_pose.z = msg->pose[i].position.z;

            received_odom_data_ = true;

            double tra = sqrt(std::pow(mav_pose.x, 2) + std::pow(mav_pose.y, 2));

            if(tra < 0.2)
            {
                mav_marker_.points.clear();
            }
        }
    }
    display_mav_tra(mav_pose);
}

void aukf_tra_pre::predicted_pose_callback(const droneMsgsROS::robotPoseStampedVector &msg)
{
    cv::Point3d pre_points;

    for(size_t i = 0; i < msg.robot_pose_vector.size(); i++)
    {
        pre_points.x = msg.robot_pose_vector[i].x;
        pre_points.y = msg.robot_pose_vector[i].y;
        pre_points.z = msg.robot_pose_vector[i].z;
        display_pre_tra(pre_points);
    }
    pre_marker_.points.clear();
}

void aukf_tra_pre::updated_pose_callback(const droneMsgsROS::dronePose &msg)
{
    cv::Point3d upd_points;
    upd_points.x = msg.x;
    upd_points.y = msg.y;
    upd_points.z = msg.z;
    display_upd_tra(upd_points);
    double tra_ = sqrt(std::pow(upd_points.x,2) + std::pow(upd_points.y,2));

    if(tra_ < 0.2)
    {
        upd_marker_.points.clear();
    }
}

void aukf_tra_pre::display_mav_tra(cv::Point3d mav_pose)
{
    geometry_msgs::Point point;
    mav_marker_.header.stamp = ros::Time();
    mav_marker_.header.frame_id = "odom";
    mav_marker_.ns = "MAV_TRA";
    mav_marker_.id = 1;
    mav_marker_.action = visualization_msgs::Marker::ADD;
    mav_marker_.type = visualization_msgs::Marker::LINE_STRIP;
    mav_marker_.pose.orientation.w = 1;
    mav_marker_.scale.x = 0.05;
    mav_marker_.color.a = 1.0;
    mav_marker_.color.r = 1;
    mav_marker_.color.g = 0;
    mav_marker_.color.b = 0;
    point.x = mav_pose.x;
    point.y = mav_pose.y;
    point.z = mav_pose.z;
    mav_marker_.points.push_back(point);
    Lemniscate_tra_pub_.publish(mav_marker_);
}

void aukf_tra_pre::display_pre_tra(cv::Point3d mav_pose)
{
    geometry_msgs::Point point;
    pre_marker_.header.stamp = ros::Time();
    pre_marker_.header.frame_id = "odom";
    pre_marker_.ns = "PRE_TRA";
    pre_marker_.id = 2;
    pre_marker_.action = visualization_msgs::Marker::ADD;
    pre_marker_.type = visualization_msgs::Marker::LINE_STRIP;
    pre_marker_.pose.orientation.w = 1;
    pre_marker_.scale.x = 0.05;
    pre_marker_.color.a = 1.0;
    pre_marker_.color.r = 1;
    pre_marker_.color.g = 1;
    pre_marker_.color.b = 0;
    point.x = mav_pose.x;
    point.y = mav_pose.y;
    point.z = mav_pose.z;
    pre_marker_.points.push_back(point);
    predicted_tra_pub_.publish(pre_marker_);
}

void aukf_tra_pre::display_upd_tra(cv::Point3d mav_pose)
{
    geometry_msgs::Point point;
    upd_marker_.header.stamp = ros::Time();
    upd_marker_.header.frame_id = "odom";
    upd_marker_.ns = "UPD_TRA";
    upd_marker_.id = 3;
    upd_marker_.action = visualization_msgs::Marker::ADD;
    upd_marker_.type = visualization_msgs::Marker::LINE_STRIP;
    upd_marker_.pose.orientation.w = 1;
    upd_marker_.scale.x = 0.05;
    upd_marker_.color.a = 1.0;
    upd_marker_.color.r = 0;
    upd_marker_.color.g = 1;
    upd_marker_.color.b = 0;
    point.x = mav_pose.x;
    point.y = mav_pose.y;
    point.z = mav_pose.z;
    upd_marker_.points.push_back(point);
    updated_tra_pub_.publish(upd_marker_);
}

void aukf_tra_pre::TrajPrediction(Eigen::VectorXd X_Pre, Eigen::MatrixXd P_Pre, Eigen::MatrixXd &F_Pre, double deltaT,
                                  std::vector<cv::Point3d> &predicted_vector, int limit_number)
{
    cv::Point3d pre_points;
    for(int i = 0; i < limit_number; i++)
    {
        aukf_.aukfPrediction(X_Pre, P_Pre, F_Pre, deltaT);
        pre_points.x = X_Pre(0);
        pre_points.y = X_Pre(2);
        pre_points.z = X_Pre(4);
        predicted_vector.push_back(pre_points);
    }
}
