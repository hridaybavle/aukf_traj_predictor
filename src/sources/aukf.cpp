#include "aukf.h"

aukf::aukf()
{
    //process noise [13*13]
    Q_.fill(0.0);
    Q_.diagonal().fill(1e-2); // << 0.5, 1e-4, 1e-6, 1e-4, 1e-6, 1e-4, 1e-4, 1e-6, 1e-4, 1e-6, 1e-4, 1e-4, 1e-6;
    //measurment noise [3*3]
    R_.fill(0.0);
    R_.diagonal() << 1e-6, 1e-6, 1e-6;

    alpha_ = 1e-3;
    beta_  = 2;
    //number of states
    n_ = 13;
    //number of measurments
    m_ = 3;
    //scaling parameter
    lambda_ = 3 - n_;
    //number of sigma points
    L_ = 2 * n_ + 1;
    //initial weights of means [1*27]
    Wm_ = Eigen::VectorXd(L_);
    //initial weights of covariance [1*27]
    Wc_ = Eigen::VectorXd(L_);
    //the first element of Wm_
    Wm_(0) = lambda_ / (lambda_ + n_);
    //the first element of Wc_
    Wc_(0) = (lambda_ / (lambda_ + n_)) + (1 - pow(alpha_, 2) + beta_);

    for (int i = 1; i < L_; i++)
    {   //the rest of 2n+1 weights elements
        Wm_(i) = 0.5 / (lambda_ + n_);
        Wc_(i) = 0.5 / (lambda_ + n_);
    }

    Xsig_pred_ = Eigen::MatrixXd(n_, L_);
}

aukf::~aukf() {}

Eigen::MatrixXd aukf::GenerateSigmaPoints(Eigen::VectorXd X, Eigen::MatrixXd P)
{
    //create sigma point matrix [13*27]
    Eigen::MatrixXd Xsig; Xsig.setZero(n_, L_);
    //calculate P sqaure root, lower triangular Cholesky factorization [13*13]
    Eigen::MatrixXd P_Cholesky = P.llt().matrixL();
    double square_root = sqrt(lambda_ + n_);
    //noisy input
    //double random = rand() % 1 + 0;
    //x = x + Q * random;

    //the first column of the sigma matrix [13*1]
    Xsig.col(0) = X;

    for (int i = 0; i < n_; i++)
    {   //columns 2 to n+1 of the sigma matrix [13*13]
        Xsig.col(i + 1) = X + square_root * P_Cholesky.col(i);
        //columns n+2 to 2n+1 of the sigma matrix [13*13]
        Xsig.col(i + 1 + n_) = X - square_root * P_Cholesky.col(i);
    }
    //overall, the sigma matrix will be [13*27]
    return Xsig;
}

void aukf::PreSigmaPoints(Eigen::MatrixXd &Xsig_pre,
                          Eigen::MatrixXd Xsig, double dt)
{
    /* Define F and H matrices
   F = [x] =       [x x_d 0  0  0  0   0          0           0         0      0   0     0   ]
       [x_d] =     [0  0  0  0  0  0   0   -sin(theta)*vel    0    cos(theta)  0   0     0   ]
       [y] =       [0  0  y y_d 0  0   0          0           0         0      0   0     0   ]
       [y_d] =     [0  0  0  0  0  0   0   cos(theta)*vel     0    sin(theta)  0   0     0   ]
       [z] =       [0  0  0  0  z z_d z_dd        0           0         0      0   0     0   ]
       [z_d] =     [0  0  0  0  0 z_d z_dd        0           0         0      0   0     0   ]
       [z_dd] =    [0  0  0  0  0  0  z_dd        0           0         0      0   0     0   ]
       [theta] =   [0  0  0  0  0  0   0        theta      theta_d      0      0   0     0   ]
       [theta_d] = [0  0  0  0  0  0   0          0           0        curv    0  vel    0   ]
       [vel] =     [0  0  0  0  0  0   0          0           0        vel    acc  0     0   ]
       [acc] =     [0  0  0  0  0  0   0          0           0         0     acc  0     0   ]
       [curv] =    [0  0  0  0  0  0   0          0           0         0      0  curv curv_d]
       [curv_d] =  [0  0  0  0  0  0   0          0           0         0      0   0   curv_d]
                                                                                             [13*13]*/
    for(int i = 0; i < L_; i++)
    {
        /* This is the non-linear model for curve estimation
      X(12) = X(12);                                     //curv_d
      X(11) = X(11) + dt * X(12);                        //curv
      X(10) = X(10);                                     //acc
      X(9) = X(9) + dt * X(10);                          //vel
      X(8) = X(9) * X(11);                               //tetha_d
      X(7) = X(7) + dt * X(8);                           //theta
      X(6) = 0;//X(6);                                   //z_dd
      X(5) = X(5) + dt * X(6);                           //z_d
      X(4) = X(4) + dt * X(5) + 0.5 * pow(dt, 2) * X(6); //z
      X(3) = sin(X(7)) * X(9);                           //y_d
      X(2) = X(2) + dt * X(3);                           //y
      X(1) = cos(X(7)) * X(9);                           //x_d
      X(0) = X(0) + dt * X(1);                           //x */

        Xsig_pre(12,i)   = Xsig(12,i) + 1e-12;                                     //curv_d
        Xsig_pre(11,i)   = Xsig(11,i) + Xsig(12,i) * dt;                           //curv
        Xsig_pre(10,i)   = Xsig(10) + 1e-11;                                       //acc
        Xsig_pre(9,i)    = Xsig(9) + Xsig(10)* dt;                                //vel
        Xsig_pre(8,i)    = Xsig(9) * Xsig(11) + 1e-10;                             //tetha_d
        Xsig_pre(7,i)    = Xsig(7) + Xsig(8) * dt;                                //theta
        Xsig_pre(6,i)    = Xsig(6);                                               //z_dd
        Xsig_pre(5,i)    = Xsig(5) + Xsig(6) * dt;                                //z_d
        Xsig_pre(4,i)    = Xsig(4) + Xsig(5) * dt + 0.5 * pow(dt, 2) * Xsig(6);    //z
        Xsig_pre(3,i)    = sin(Xsig_pre(7)) * Xsig_pre(9);                         //y_d (using Xsig_pre(7), Xsig_pre(9) not a bug)
        Xsig_pre(2,i)    = Xsig(2) + Xsig(3) * dt;                                 //y
        Xsig_pre(1,i)    = cos(Xsig_pre(7)) * Xsig_pre(9);                         //x_d (using Xsig_pre(7), Xsig_pre(9) not a bug)
        Xsig_pre(0,i)    = Xsig(0) + Xsig(1) * dt;                                 //x */

        Xsig_pre.col(i) = Xsig_pre.col(i);
    }
}

void aukf::AdaptiveUKF(Eigen::MatrixXd Xsig_pre, Eigen::VectorXd &X, Eigen::MatrixXd &P)
{
    //[13*1]
    //X.fill(0.0);
    //[13*13]
    //P.fill(0.0);

    //    for(int i = 0; i < L_; i++)
    //    {   //iterate over sigma points
    //        X = X + Wm_(i) * Xsig_pre.col(i);
    //    }

    Eigen::MatrixXd Wm = Wm_.transpose().replicate(Xsig_pre.rows(), 1);
    Eigen::MatrixXd Xsig_pred_weighted = (Xsig_pre.array() * Wm.array()).matrix();
    X = Xsig_pred_weighted.rowwise().sum();

//    for(int i = 0; i < L_; i++)
//    {   //iterate over sigma points
//        Eigen::VectorXd x_diff = Xsig_pre.col(i) - X;
//        P = P + Wc_(i) * x_diff * x_diff.transpose();
//    }

//    P = P + Q_;

    Eigen::MatrixXd Wc = Wc_.transpose().replicate(Xsig_pre.rows(), 1);
    Eigen::MatrixXd X_mean_distance = (Xsig_pre.colwise() - X);
    Eigen::MatrixXd X_weighted_mean_distance = (X_mean_distance.array() * Wc.array()).matrix();
    P = X_weighted_mean_distance * X_mean_distance.transpose() + Q_;
}

void aukf::aukfPrediction(Eigen::VectorXd &X, Eigen::MatrixXd &P, Eigen::MatrixXd &F, double dt)
{
    Eigen::MatrixXd Xsig;
    Eigen::MatrixXd Xsig_pre; Xsig_pre.setZero(n_, L_);
    Xsig = GenerateSigmaPoints(X, P);
    PreSigmaPoints(Xsig_pre, Xsig, dt);
    AdaptiveUKF(Xsig_pre, X, P);
}

/*void aukf::Tra_Iteration(Eigen::VectorXd &X, double dt)
{
    X(12) = X(12);                                     //curv_d
    X(11) = X(11) + dt * X(12);                        //curv
    X(10) = X(10);                                     //acc
    X(9) = X(9) + dt * X(10);                          //vel
    X(8) = X(9) * X(11);                               //tetha_d
    X(7) = X(7) + dt * X(8);                           //theta
    X(6) = 0;//X(6);                                   //z_dd
    X(5) = X(5) + dt * X(6);                           //z_d
    X(4) = X(4) + dt * X(5) + 0.5 * pow(dt, 2) * X(6); //z
    X(3) = sin(X(7)) * X(9);                           //y_d
    X(2) = X(2) + dt * X(3);                           //y
    X(1) = cos(X(7)) * X(9);                           //x_d
    X(0) = X(0) + dt * X(1);                           //x
}*/

void aukf::aukfUpdate(Eigen::VectorXd &X, Eigen::MatrixXd &P, Eigen::VectorXd &X_Measure)
{
    Eigen::MatrixXd Xsig;
    Xsig = GenerateSigmaPoints(X, P);
    Eigen::MatrixXd Zsig(m_, L_);

    for(int i = 0; i < L_; i++)
    {   //iterate over sigma points [3*27]
        Zsig(0,i) = Xsig(0,i);
        Zsig(1,i) = Xsig(2,i);
        Zsig(2,i) = Xsig(4,i);
    }

    Eigen::VectorXd z_pred = Eigen::VectorXd(m_);
    z_pred.fill(0.0);

    for (int i = 0; i < L_; i++)
    {
        z_pred = z_pred + Wm_(i) * Zsig.col(i);
    }

    //measurement covariance [3*3]
    Eigen::MatrixXd Pzz = Eigen::MatrixXd(m_, m_);
    Pzz.fill(0.0);
    //transformed cross-covariance [13*3]
    Eigen::MatrixXd Pxz = Eigen::MatrixXd(n_,m_);
    Pxz.fill(0.0);

    for (int i = 0; i < L_; i++)
    {   //measurments difference [3*1]
        Eigen::VectorXd z_diff = Zsig.col(i) - z_pred;
        Pzz = Pzz + Wc_(i) * z_diff * z_diff.transpose();
        //states difference
        Eigen::VectorXd x_diff = Xsig.col(i) - X;
        Pxz = Pxz + Wc_(i) * x_diff * z_diff.transpose();
    }

    Pzz = Pzz + R_;

    //compute Kalman gain
    Eigen::MatrixXd K = Eigen::MatrixXd(n_, m_);
    K = Pxz * Pzz.inverse();
    X = X + K * (X_Measure - z_pred);
    P = P - K * Pzz * K.transpose();

    //    //update Q and R matrices
    //    Eigen::VectorXd d_z = Eigen::VectorXd(m_);
    //    Eigen::MatrixXd H = Eigen::MatrixXd(m_, n_);
    //    Eigen::MatrixXd S = Eigen::MatrixXd(m_, m_);
    //    Eigen::MatrixXd C_k = Eigen::MatrixXd(m_, m_);
    //    d_z = X_Measure - z_pred;
    //    //------------------
    //    C_k.fill(0.0);

    //    for (int i = 0; i < L_; i++)
    //    {
    //        Eigen::VectorXd z_diff = Zsig.col(i) - z_pred;
    //        C_k = C_k + z_diff * z_diff.transpose();
    //        R_ = C_k + Wc_(i) * z_diff * z_diff.transpose();
    //    }

    //    Q_ = K * C_k * K.transpose();
    //------------------
    //    //calculate Mahalanobis distance
    //    double M_k = sqrt(d_z.transpose() * Pzz.inverse() * d_z);
    //    R_ = R_ * pow(M_k, 2);
    //------------------
    //    H.fill(0.0);
    //    H(0,0) = 1;
    //    H(1,1) = 1;
    //    H(2,2) = 1;
    //    double a = 0.9;
    //    S = H * P * H.transpose();
    //    //adapt Q based on the redsidual and forgetting factor
    //    Q_ = a * Q_ + (1 - a) * (K * d_z * d_z.transpose() * K.transpose());
    //    //adapt R based on the redsidual and forgetting factor
    //    R_ = a * R_ + (1 - a) * (d_z * d_z.transpose() + S);
}
